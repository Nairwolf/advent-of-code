package main

import (
	"fmt"
	"gitlab.com/Nairwolf/advent-of-code/utils"
	"strings"
)

func main() {
	commands := utils.ReadLines("input.txt")

	{
		fmt.Println("--- Part One ---")
		finalCoord := driveSubmarine(commands, false)
		horizontal := finalCoord.horizontal
		depth := finalCoord.depth
		fmt.Printf("horizontal = %v; depth = %v\n", horizontal, depth)
		fmt.Printf("result part1 = %+v\n", horizontal*depth)
	}

	{
		fmt.Println("--- Part Two ---")
		finalCoord := driveSubmarine(commands, true)
		horizontal := finalCoord.horizontal
		depth := finalCoord.depth
		fmt.Printf("horizontal = %v; depth = %v\n", horizontal, depth)
		fmt.Printf("result part2 = %v\n", finalCoord.horizontal*finalCoord.depth)
	}
}

type Coord struct {
	horizontal int
	depth      int
	aim        int
}

func (c *Coord) move(cmd string, isV2 bool) {
	cmdSplit := strings.Split(cmd, " ")
	action := cmdSplit[0]
	val := utils.ToInt(cmdSplit[1])
	if isV2 {
		switch action {
		case "forward":
			c.horizontal = c.horizontal + val
			c.depth = c.depth + val*c.aim
		case "up":
			c.aim = c.aim - val
		case "down":
			c.aim = c.aim + val
		}
	} else {
		switch action {
		case "forward":
			c.horizontal = c.horizontal + val
		case "up":
			c.depth = c.depth - val
		case "down":
			c.depth = c.depth + val
		}
	}
}

func driveSubmarine(commands []string, isV2 bool) *Coord {
	c := &Coord{}
	for _, cmd := range commands {
		c.move(cmd, isV2)
	}
	return c
}

package main

import (
    "fmt"
    "os"
    "bufio"
    "strconv"
    "strings"
    "math"
)

type Position struct {
    x, y int
}

type Grid struct {
    grid map[Position]int
}


func main() {
    lines := readLines("input.txt")
    result := findClosestIntersectionPoint(lines)
    fmt.Println(result)
}

func findClosestIntersectionPoint(wires []string) int {
    firstWire := wires[0]
    secondWire := wires[1]

    firstGrid := make(map[Position]int)
    tokens := strings.Split(firstWire, ",")
    currPos := Position{}

    for _, token := range tokens {
        finalPos := movePointOnGrid(token, currPos, firstGrid)
        currPos = finalPos
    }
    fmt.Println(firstGrid)

    secondGrid := make(map[Position]int)
    tokens = strings.Split(secondWire, ",")
    currPos = Position{}
    for _, token := range tokens {
        finalPos := movePointOnGrid(token, currPos, secondGrid)
        currPos = finalPos
    }
    fmt.Println(secondGrid)

    findIntersections(firstGrid, secondGrid)

    return 159
}

func findIntersections(grid1, grid2 map[Position]int) {
    for pos, _ := range grid1 {
        if grid2[pos] == 1 {
            fmt.Println(pos)
            fmt.Println(ManhattanDistance(pos))
        }
    }
}

func ManhattanDistance(pos Position) int {
    return abs(pos.x) + abs(pos.y)
}

func movePointOnGrid(token string, initPos Position, grid map[Position]int) Position {
    dir := string(token[0])
    length := toInt(token[1:])
    finalPos := initPos
    for i := 0; i < length ; i ++ {
        switch dir {
        case "R":
            finalPos.x += 1
            grid[finalPos] = 1
        case "L":
            finalPos.x -= 1
            grid[finalPos] = 1
        case "U":
            finalPos.y += 1
            grid[finalPos] = 1
        case "D":
            finalPos.y -= 1
            grid[finalPos] = 1
        }
    }

    return finalPos
}

//func parseToken(token []string) string, int {
//    var dir string
//    switch token[0] {
//    case 'R':
//        dir = 'R'
//    case 'L':
//        dir = 'L'
//    case 'U':
//        fmt.Println("Up")
//    case 'D':
//        fmt.Println("Down")
//    }
//
//}


func readLines(filename string) []string {
    file, err := os.Open(filename)
    check(err)
    defer file.Close()

    scanner := bufio.NewScanner(file)

    var lines []string
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines
}

func toInt(s string) int {
    val, err := strconv.Atoi(s)
    check(err)
    return val
}

func check(err error) {
    if err != nil {
        panic(err)
    }
}

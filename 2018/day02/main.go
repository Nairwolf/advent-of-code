package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func part1(lines []string) {
	// You have a list of IDs
	// Count the number of IDs containing exactly two of any letter
	// Count the number of IDs containing exactly three of any letter
	// Result is the multiplication of both number
	counters := map[int]int{2: 0, 3: 0}

	for _, line := range lines {
		lineCounter := map[string]int{}

		for _, char := range line {
			lineCounter[string(char)] += 1
		}

		hasSeenTwo, hasSeenThree := false, false

		for _, nb := range lineCounter {
			if nb == 2 && !hasSeenTwo {
				counters[2] += 1
				hasSeenTwo = true
			} else if nb == 3 && !hasSeenThree {
				counters[3] += 1
				hasSeenThree = true
			}
		}
	}

	result := counters[2] * counters[3]
	fmt.Printf("result part1 is: %v\n", result)
}

func part2(lines []string) {
	// You have a list of IDs
	// Only two IDs differ by exactly one character
	// Result is ID by removing the different character
	nb_lines := len(lines)
	for i := 0; i < nb_lines-1; i++ {

		for j := i + 1; j < nb_lines-1; j++ {
			rst, index := hasOneDiff(lines[i], lines[j])

			if rst {
				finalLine := lines[i][:index] + lines[i][index+1:]
				fmt.Printf("result part2 is: %v\n", finalLine)
			}
		}
	}
}

func hasOneDiff(lineA, lineB string) (bool, int) {
	var hasOneDiff bool
	nb_difference := 0
	index := 0

	for i := 0; i < len(lineA) && nb_difference < 2; i++ {
		if lineA[i] != lineB[i] {
			nb_difference += 1
			index = i
		}
	}

	if nb_difference == 1 {
		hasOneDiff = true
	}

	return hasOneDiff, index
}

func main() {
	raw, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	lines := strings.Split(string(raw), "\n")
	part1(lines)
	part2(lines)
}

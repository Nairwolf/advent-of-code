#!/usr/bin/env python3

def exo1(content):
    total_fuel = 0
    for mass in content.splitlines():
        fuel = int(mass) // 3 - 2
        total_fuel += fuel

    print(total_fuel)

def exo2(content):
    rest_fuel = 0
    for mass in content.splitlines():
        fuel = int(mass) // 3 - 2
        rest_fuel += fuel_needed(fuel)
    print(rest_fuel)

def fuel_needed(fuel):
    if fuel < 0:
        return 0
    return fuel + fuel_needed(fuel // 3 - 2)

if __name__ == '__main__':
    with open("input.txt", "r") as fo:
        content = fo.read()

    exo1(content)
    exo2(content)

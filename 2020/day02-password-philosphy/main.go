package main

import (
	"fmt"
	"regexp"
	"strings"

	"gitlab.com/Nairwolf/advent-of-code/utils"
)

func part1(lines []string, re *regexp.Regexp) int {
	var nbPwdValid int
	for _, line := range lines {
		parts := re.FindStringSubmatch(line)
		min := utils.ToInt(parts[1])
		max := utils.ToInt(parts[2])
		letter := parts[3]
		password := parts[4]
		nb := strings.Count(password, letter)
		if min <= nb && nb <= max {
			nbPwdValid++
		}
	}
	return nbPwdValid
}

func part2(lines []string, re *regexp.Regexp) int {
	var nbPwdValid int
	for _, line := range lines {
		parts := re.FindStringSubmatch(line)
		firstPos := utils.ToInt(parts[1])
		secondPos := utils.ToInt(parts[2])
		letter := parts[3]
		password := parts[4]

		//	------------------
		//	| a | b | a != b |
		//	-----------------|
		//	| 0 | 0 |   0    |
		//	| 0 | 1 |   1    |
		//	| 1 | 0 |   1    |
		//	| 1 | 1 |   0    |
		//	------------------
		pos1HasLetter := string(password[firstPos-1]) == letter
		pos2HasLetter := string(password[secondPos-1]) == letter

		if pos1HasLetter != pos2HasLetter {
			nbPwdValid++
		}
	}
	return nbPwdValid
}

func main() {
	lines := utils.ReadLines("input.txt")
	re := regexp.MustCompile(`^(\d+)-(\d+) ([a-z]): ([a-z]+)$`)
	{
		fmt.Println("--- Part One ---")
		nbPwdValid := part1(lines, re)
		fmt.Printf(" result part1 = %+v\n", nbPwdValid)
	}
	{
		fmt.Println("--- Part Two ---")
		nbPwdValid := part2(lines, re)
		fmt.Printf(" result part2 = %+v\n", nbPwdValid)
	}
}

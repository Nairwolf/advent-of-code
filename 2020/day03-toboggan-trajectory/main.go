package main

import (
	"fmt"

	"gitlab.com/Nairwolf/advent-of-code/utils"
)

type Coord struct {
	X int
	Y int
}

func parseTreeMap(lines []string) map[Coord]bool {
	treeMap := make(map[Coord]bool)
	for y, line := range lines {
		for x, char := range line {
			if string(char) == "#" {
				treeMap[Coord{x + 1, y + 1}] = true
			}
		}
	}
	return treeMap
}

func navigateThroughTreeMap(treeMap map[Coord]bool, length int, width int, slope Coord) (nbTree int) {
	pos := Coord{1, 1}
	for {
		pos = Coord{pos.X + slope.X, pos.Y + slope.Y}
		if pos.Y > length {
			break
		}
		if pos.X > width {
			newX := pos.X % width
			if newX == 0 {
				pos = Coord{width, pos.Y}
			} else {
				pos = Coord{newX, pos.Y}
			}
		}
		if treeMap[pos] {
			nbTree++
		}
	}
	return
}

func main() {
	lines := utils.ReadLines("input.txt")

	treeMap := parseTreeMap(lines)

	length := len(lines)
	width := len(lines[0])

	{
		fmt.Println("--- Part One ---")
		slope := Coord{3, 1}
		nbTree := navigateThroughTreeMap(treeMap, length, width, slope)
		fmt.Printf("result part1 = %v\n", nbTree)
	}

	{
		fmt.Println("--- Part Two ---")
		slopes := []Coord{Coord{1, 1}, Coord{3, 1}, Coord{5, 1}, Coord{7, 1}, Coord{1, 2}}
		result := 1
		for _, slope := range slopes {
			nbTree := navigateThroughTreeMap(treeMap, length, width, slope)
			//fmt.Printf("slope: %v / nbTree: %v\n", slope, nbTree)
			result *= nbTree
		}
		fmt.Printf("result part2 = %v\n", result)
	}

}

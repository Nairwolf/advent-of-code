package main

import (
    "fmt"
    "io/ioutil"
    "log"
    "strings"
    "regexp"
    "strconv"
)

type Coord struct {
    X int
    Y int
}

type Dim struct {
    X int
    Y int
}

type Fabric struct {
    id string
    coord Coord
    dim Dim
}

type Result struct {
    nb int
    ids []string
}

const MAX  = 1000

func parseFabric(line string) Fabric {
    var re = regexp.MustCompile(`#(\d+) @ (\d+),(\d+): (\d+)x(\d+)`)
    var fabric Fabric

    match := re.FindStringSubmatch(line)
    if match != nil {
        fabric.id = match[1]

        coordX, _ := strconv.Atoi(match[2])
        coordY, _ := strconv.Atoi(match[3])
        dimX, _ := strconv.Atoi(match[4])
        dimY, _ := strconv.Atoi(match[5])

        fabric.coord = Coord{coordX, coordY}
        fabric.dim = Dim{dimX, dimY}
    }

    return fabric
}

func incFabric(fabric Fabric, m map[Coord]Result) {
    startX := fabric.coord.X
    endX := startX + fabric.dim.X
    startY := fabric.coord.Y
    endY := startY + fabric.dim.Y

    for i := startX; i < endX && i < MAX; i++ {
        for j := startY; j < endY && j < MAX; j++ {

            coord := Coord{i, j}
            result := m[coord]

            // For part1, only nb is useful information
            // To solve part2, I had to add a list of ids in the Result struct
            newResult := Result{
                nb: result.nb + 1,
                ids: append(result.ids, fabric.id),
            }
            m[coord] = newResult
        }
    }
}

func part1(lines []string) map[Coord]Result {
    // A claim like #123 @ 3,2: 5x4 means that claim ID 123 specifies
    // a rectangle 3 inches from the left edge, 2 inches from the top edge,
    // 5 inches wide, and 4 inches tall.
    // How many square inches of fabric are within two or more claims?
    m := make(map[Coord]Result, MAX*MAX)

    for _, line := range lines {
        fabric := parseFabric(line)
        incFabric(fabric, m)
    }

    count := 0
    for _, result := range m {
        if result.nb >= 2 {
            count += 1
        }
    }

    fmt.Printf("result part1 is: %v\n", count)
    return m
}

func part2(nb_ids int, m map[Coord]Result) {
    // What is the ID of the only claim that doesn't overlap?
    hasMultipleClaim := make(map[string]bool)

    for _, result := range m {
        if result.nb >= 2 {

            for _, id := range result.ids {
                hasMultipleClaim[id] = true
            }
        }
    }

    for i:= 1; i < nb_ids; i++ {
        if !hasMultipleClaim[strconv.Itoa(i)] {
            fmt.Printf("result part2 is: %v\n", i)
            return
        }
    }
}

func main() {
    raw, err := ioutil.ReadFile("input.txt")
    if err != nil {
        log.Fatal(err)
    }

    lines := strings.Split(string(raw), "\n")
    m := part1(lines)
    part2(len(lines), m)
}


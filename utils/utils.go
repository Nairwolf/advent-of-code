package utils

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func ReadLines(filename string) []string {
	file, err := os.Open(filename)
	check(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)

	var lines []string
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines
}

func ReadNumbers(filename string) []int {
	file, err := os.Open(filename)
	check(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)

	var numbers []int
	for scanner.Scan() {
		numbers = append(numbers, ToInt(scanner.Text()))
	}
	return numbers
}

func ReadParagraphs(filename string) []string {
	file, err := os.Open(filename)
	check(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var paragraphs []string
	var currentParagraph []string
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Printf("line = %+v\n", line)
		if line == "" {
			paragraph := strings.Join(currentParagraph, " ")
			paragraphs = append(paragraphs, paragraph)
			currentParagraph = make([]string, 0)
		}
		currentParagraph = append(currentParagraph, line)
	}
	lastParagraph := strings.Join(currentParagraph, " ")
	paragraphs = append(paragraphs, lastParagraph)
	return paragraphs
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func ToInt(s string) int {
	result, err := strconv.Atoi(s)
	check(err)
	return result
}

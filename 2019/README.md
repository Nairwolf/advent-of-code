# Adevnt of Code 2019

# Solutions

* https://github.com/raginjason/aoc2019
* https://github.com/stevotvr/adventofcode2019
* https://git.sr.ht/~hokiegeek/adventofcode/tree/master
* https://github.com/marcosfede/algorithms/tree/master/adventofcode/2019

# Tips and Tricks

* https://medium.com/@danvdk/python-tips-tricks-for-the-advent-of-code-2019-89ec23a595dd

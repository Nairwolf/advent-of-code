package main

import (
	"fmt"
	"regexp"

	"gitlab.com/Nairwolf/advent-of-code/utils"
)

type Passport struct {
	byr string
	iyr string
	eyr string
	hgt string
	hcl string
	ecl string
	pid string
	cid string
}

func newPassport(passport string) *Passport {
	p := &Passport{}
	byr := regexp.MustCompile(`byr:(\S+)`).FindStringSubmatch(passport)
	if len(byr) > 1 {
		p.byr = byr[1]
	}

	iyr := regexp.MustCompile(`iyr:(\S+)`).FindStringSubmatch(passport)
	if len(iyr) > 1 {
		p.iyr = iyr[1]
	}

	eyr := regexp.MustCompile(`eyr:(\S+)`).FindStringSubmatch(passport)
	if len(eyr) > 1 {
		p.eyr = eyr[1]
	}

	hgt := regexp.MustCompile(`hgt:(\S+)`).FindStringSubmatch(passport)
	if len(hgt) > 1 {
		p.hgt = hgt[1]
	}

	hcl := regexp.MustCompile(`hcl:(\S+)`).FindStringSubmatch(passport)
	if len(hcl) > 1 {
		p.hcl = hcl[1]
	}

	ecl := regexp.MustCompile(`ecl:(\S+)`).FindStringSubmatch(passport)
	if len(ecl) > 1 {
		p.ecl = ecl[1]
	}

	pid := regexp.MustCompile(`pid:(\S+)`).FindStringSubmatch(passport)
	if len(pid) > 1 {
		p.pid = pid[1]
	}

	//cid := regexp.MustCompile(`cid:(\S+)`).FindStringSubmatch(passport)
	//p.cid = cid[0]

	return p
}

func (p *Passport) isValid() bool {
	return p.byr != "" &&
		p.iyr != "" &&
		p.eyr != "" &&
		p.hgt != "" &&
		p.hcl != "" &&
		p.ecl != "" &&
		p.pid != ""
}

func main() {
	var nbValidPassport int
	var nbInvalidPassport int
	paragraphs := utils.ReadParagraphs("input.txt")
	fmt.Printf("len(paragraphs) = %+v\n", len(paragraphs))

	for _, paragraph := range paragraphs {
		fmt.Printf("paragraph = %+v\n", paragraph)
		passport := newPassport(paragraph)
		if passport.isValid() {
			//fmt.Println("password valid")
			//fmt.Printf("password = %+v\n", passport)
			nbValidPassport++
		} else {
			//fmt.Println("password invalid")
			//fmt.Printf("paragraph = %+v\n", paragraph)
			//fmt.Printf("password = %+v\n", passport)
			nbInvalidPassport++
		}
	}
	fmt.Println(nbValidPassport)
	fmt.Println(nbInvalidPassport)
}

package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func exo1(masses []string) {
	// Fuel required to launch a given module is based on its mass.
	// Specifically, to find the fuel required for a module,
	// take its mass, divide by three, round down, and subtract 2.
	total_fuel := 0
	for _, mass := range masses {
		mass, _ := strconv.Atoi(mass)
		fuel := mass/3 - 2
		total_fuel += fuel
	}

	fmt.Println(total_fuel)

}

func exo2(masses []string) {
	// Fuel also need fuel with the same computation
	total_fuel := 0
	for _, mass := range masses {
		mass, _ := strconv.Atoi(mass)
		fuel := mass/3 - 2
		total_fuel += fuel_needed(fuel)
	}

	fmt.Println(total_fuel)

}

func fuel_needed(fuel int) int {
	if fuel < 0 {
		return 0
	}

	return fuel + fuel_needed(fuel/3-2)
}

func main() {
	raw, err := ioutil.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}
	masses := strings.Split(string(raw), "\n")

	exo1(masses)
	exo2(masses)
}

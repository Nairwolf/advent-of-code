package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func run_intCodeProgram(input []string) {
	for i := 0; i < len(input)-4; i += 4 {
		rst := compute(i, input)
		if rst == 99 {
			break
		}
	}
}

func compute(index int, input []string) int {
	intCode := input[index : index+4]
	opCode := intCode[0]
	pos1, _ := strconv.Atoi(intCode[1])
	pos2, _ := strconv.Atoi(intCode[2])
	pos3, _ := strconv.Atoi(intCode[3])

	val1, _ := strconv.Atoi(input[pos1])
	val2, _ := strconv.Atoi(input[pos2])
	rst := 0
	if opCode == "1" {
		// add
		rst = val1 + val2
	} else if opCode == "2" {
		// multiply
		rst = val1 * val2
	} else if opCode == "99" {
		return 99
	}
	input[pos3] = strconv.Itoa(rst)
	return 0
}

func exo1(input []string) {
	// replace position 1 with the value 12 and replace position 2 with the value 2
	input[1] = "12"
	input[2] = "2"

	run_intCodeProgram(input)

	// What value is left at position 0 ?
	fmt.Println(input[0])
}

func exo2(input []string) {
	output_expected := 19690720
	output := 0

	candidate := make([]string, len(input))
	//for ok := true; ok; ok = output == output_expected {
	for noun := 0; noun <= 99; noun++ {
		for verb := 0; verb <= 99; verb++ {
			_ = copy(candidate, input)
			candidate[1] = strconv.Itoa(noun)
			candidate[2] = strconv.Itoa(verb)
			run_intCodeProgram(candidate)
			output, _ = strconv.Atoi(candidate[0])
			if output == output_expected {
				fmt.Println(100*noun + verb)
				break
			}
		}
	}
	//fmt.Println(candidate[0])

}

func main() {
	raw, err := ioutil.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}
	input := strings.Split(string(raw), ",")
	input2 := make([]string, len(input))
	_ = copy(input2, input)

	exo1(input)
	exo2(input2)
}

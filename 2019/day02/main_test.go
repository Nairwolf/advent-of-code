package main

import (
    "testing"
    "strings"
)

func TestRunIntCodeProgram(t *testing.T) {
    var tests = []struct {
        raw, want string
    }{
        {"1,9,10,3,2,3,11,0,99,30,40,50", "3500,9,10,70,2,3,11,0,99,30,40,50"},
        {"1,0,0,0,99", "2,0,0,0,99"},
        {"2,3,0,3,99", "2,3,0,6,99"},
        {"2,4,4,5,99,0", "2,4,4,5,99,9801"},
        {"1,1,1,4,99,5,6,0,99", "30,1,1,4,2,5,6,0,99"},
    }

    for _, tt := range tests {
        input := strings.Split(string(tt.raw), ",")
        run_intCodeProgram(input)
        output := strings.Join(input, ",")
        if output != tt.want {
            t.Errorf("got %v, want %v", output, tt.want)
        }

    }
}

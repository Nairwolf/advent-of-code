package main

import (
	"fmt"
	"gitlab.com/Nairwolf/advent-of-code/utils"
)

func main() {
	numbers := utils.ReadNumbers("input.txt")

	{
		fmt.Println("--- Part One ---")
		nbIncreases := countNbIncreases(numbers)
		fmt.Printf("result part1 = %+v\n", nbIncreases)
	}

	{
		fmt.Println("--- Part Two ---")
		nbIncreases := countIncreasesSlidingWindow(numbers)
		fmt.Printf("result part2 = %+v\n", nbIncreases)

	}
}

func countNbIncreases(numbers []int) (nbIncreases int) {
	l := len(numbers)
	for i := 1; i < l; i++ {
		prev := numbers[i-1]
		actual := numbers[i]
		if actual-prev > 0 {
			nbIncreases += 1
		}
	}
	return nbIncreases
}

func countIncreasesSlidingWindow(numbers []int) (nbIncreases int) {
	l := len(numbers)
	sum := func(X int) int {
		return numbers[X] + numbers[X+1] + numbers[X+2]
	}
	for i := 0; i < l-3; i++ {
		actualSum := sum(i)
		nextSum := sum(i + 1)
		if nextSum-actualSum > 0 {
			nbIncreases += 1
		}
	}
	return nbIncreases
}

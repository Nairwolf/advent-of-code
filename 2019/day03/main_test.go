package main

import (
	"testing"
)

func TestExo1(t *testing.T) {
	var tests = []struct {
		input    []string
		expected int
	}{
        {[]string{"R2,U3,L3", "R2,D10"}, 0},
		//{[]string{"R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83"}, 159},
		//{[]string{"R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"}, 135},
	}

	for _, tt := range tests {
		output := findClosestIntersectionPoint(tt.input)
		if output != tt.expected {
			t.Errorf("got %v, want %v", output, tt.expected)
		}
	}
}

func TestMovePointOnGrid(t *testing.T) {
    var tests = []struct {
        token string
        initPos Position
        expected Position
    }{
        {"R2", Position{}, Position{2, 0}},
        {"U10", Position{}, Position{0, 10}},
        {"L5", Position{5, 1}, Position{0, 1}},
    }

    for _, tt := range tests {
        grid := make(map[Position]int)
        finalPos := movePointOnGrid(tt.token, tt.initPos, grid)
        if finalPos != tt.expected {
            t.Errorf("got %v, want %v", finalPos, tt.expected)
        }
    }
}

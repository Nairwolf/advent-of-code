package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
)

func exo1() {
	file, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	frequency := 0

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		number, err := strconv.Atoi(scanner.Text())
		if err == nil {
			frequency = frequency + number
		}
	}
	fmt.Println("Final value")
	fmt.Println(frequency)
}

func exo2() {
	content, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	visited := map[int]bool{0: true}
	sumFrequency := 0

	freqChanges := strings.Split(string(content), "\n")

	for {
		for _, value := range freqChanges {
			number, err := strconv.Atoi(value)
			if err == nil {
				sumFrequency += number

				if visited[sumFrequency] {
					fmt.Println(sumFrequency)
					fmt.Println("is already visited")
                    return
				}

				visited[sumFrequency] = true
			}
		}
	}
}

func main() {
	exo1()
	exo2()
}

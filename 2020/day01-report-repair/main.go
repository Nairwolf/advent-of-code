package main

import (
	"fmt"
	"math"

	"gitlab.com/Nairwolf/advent-of-code/utils"
)

func main() {
	numbers := utils.ReadNumbers("input.txt")
	valueToFind := 2020

	{
		fmt.Println("--- Part One ---")
		_, result := findTwoNumbers(valueToFind, numbers)
		fmt.Printf("a = %+v; b = %+v\n", result[0], result[1])
		fmt.Printf("result part1 = %+v\n", result[0]*result[1])
	}

	{
		fmt.Println("--- Part Two ---")
		for i := 0; i < len(numbers); i++ {
			a := numbers[i]
			found, result := findTwoNumbers(valueToFind-a, numbers[i:])
			if found {
				b := result[0]
				c := result[1]
				fmt.Printf("a = %+v; b = %+v; c = %+v\n", a, b, c)
				fmt.Printf("result part2 = %+v\n", a*b*c)
			}

		}
	}
}

func findTwoNumbers(value int, numbers []int) (found bool, result []int) {
	middle := int(math.Round(float64(len(numbers) / 2)))
	for i := 0; i < middle; i++ {
		entry1 := numbers[i]
		for j := i + 1; j < len(numbers); j++ {
			entry2 := numbers[j]
			if entry1+entry2 == value {
				found = true
				result = []int{entry1, entry2}
				break
			}
		}
	}
	return
}
